public class Card{
	//fields
	private Suit suit;
	private Rank rank;
	
	//constructor
	public Card(Suit suit, Rank rank){
		this.suit = suit;
		this.rank = rank;
	}
	//get methods
	public double getSuit(){
		return this.suit.getSuitOfCard();
	}
	public double getRank(){
		return this.rank.getRankOfCard();
	}
	//toString method returning a String
	public String toString(){
		if(this.rank.getRankOfCard() == 11){
			return "Jack of " + this.suit;
		}else if(this.rank.getRankOfCard() == 12){
			return "Queen of " + this.suit;
		}else if(this.rank.getRankOfCard() == 13){
			return "King of " + this.suit;
		}else if(this.rank.getRankOfCard() == 1){
			return "Ace of " + this.suit;
		}else{
			return this.rank + " of " + this.suit;
		}
	}
	//Calculates score based on suit and value
	public double calculateScore(){
		return this.rank.getRankOfCard() + this.suit.getSuitOfCard();
	}
}