import java.util.Random;
public class Deck{
	//fields
	private Card[] cards;
	private int numberOfCards;
	private Random rng;
	
	//Assigns amount of cards in deck
	//Creares values for suit and value
	//Places cards in original order of a simple deck
	public Deck(){
		// this.numberOfCards = 52;
		// this.rng = new Random();
		// this.cards = new Card[52];
		// String[] suits = {"Hearts", "Diamonds", "Clubs", "Spades"};
		// double[] values = {1.0,2.0,3.0,4.0,5.0,6.0,7.0,8.0,9.0,10.0,11.0,12.0,13.0};
		// int index = 0;
		// for(String suit:suits){
			// for(double value:values){
				// cards[index] = new Card(suit,value);
				// index++;
			// }
			
		// }
		this.numberOfCards = 52;
		this.rng = new Random();
		this.cards = new Card[52];
		int index = 0;
		Suit[] suits = {Suit.HEARTS, Suit.SPADES, Suit.DIAMONDS, Suit.CLUBS};
		Rank[] ranks = {Rank.ACE, Rank.KING, Rank.QUEEN, Rank.JACK, Rank.TEN, Rank.NINE, Rank.EIGHT, Rank.SEVEN, Rank.SIX, Rank.FIVE, Rank.FOUR, Rank.THREE, Rank.TWO};
		for(Suit elementSuit:suits){
			for(Rank elementRank:ranks){
				cards[index] = new Card(elementSuit, elementRank);
				index++;
			}
		}
	}
	public int length(){
		return numberOfCards;
	}
	public Card drawToCard(){
		this.numberOfCards--;
		return this.cards[numberOfCards];
	}
	public String toString(){
		String allCards = "";
		for(int i=0; i<cards.length; i++){
			allCards += cards[i].toString();
			allCards += ", ";
		}
		return allCards;
	}
	public Card[] shuffle(){
		for(int i =0; i<numberOfCards; i++){
		int randomPosition = rng.nextInt(cards.length - i);
			Card temp = cards[randomPosition];
			cards[randomPosition] = cards[i];
			cards[i] = temp;
		}
		return cards;
	}
}