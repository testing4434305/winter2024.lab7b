public enum Rank{
	KING(13.0),
	QUEEN(12.0),
	JACK(11.0),
	TEN(10.0),
	NINE(9.0),
	EIGHT(8.0),
	SEVEN(7.0),
	SIX(6.0),
	FIVE(5.0),
	FOUR(4.0),
	THREE(3.0),
	TWO(2.0),
	ACE(1.0);
	
	//field
	private final double rankOfCard;
	
	//constructor
	Rank(double rankOfCard){
		this.rankOfCard = rankOfCard;
	}
	public double getRankOfCard(){
		return this.rankOfCard;
	}
}