public class SimpleWar{
	public static void main(String[] args){
		Deck game1 = new Deck();
		game1.shuffle();
		//ints to store scores after each round
		int pointsOfPlayer1 = 0;
		int pointsOfPlayer2 = 0;
		//while the deck has more than 2 cards (one for each player)the game will continue
		while(game1.length()>1){
			//storing cards for each round
			Card cardOfPlayerOne = game1.drawToCard();
			Card cardOfPlayerTwo = game1.drawToCard();
			double scoreOfPlayerOne = cardOfPlayerOne.calculateScore();
			double scoreOfPlayerTwo = cardOfPlayerTwo.calculateScore();
			System.out.println("The card is: " + cardOfPlayerOne + " And it's value is: " + scoreOfPlayerOne);
			System.out.println("The card is: " + cardOfPlayerTwo + " And it's value is: " + scoreOfPlayerTwo);
			//compare the values of cards
			if(scoreOfPlayerTwo>scoreOfPlayerOne){
				System.out.println("Player two wins the round");
				pointsOfPlayer2++;
			}else{
				System.out.println("Player one wins the round");
				pointsOfPlayer1++;
			}
		}
		//Congratulations based on who has more points
		if(pointsOfPlayer1>pointsOfPlayer2){
			System.out.println("Player one wins the game!");
		}else{
			System.out.println("Player two wins the game!");
		}
	}
}