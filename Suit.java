public enum Suit{
	HEARTS(0.4),
	SPADES(0.3),
	DIAMONDS(0.2),
	CLUBS(0.1);
	
	//field
	private final double suitOfCard;
	
	//constructor
	Suit(double suitOfCard){
		this.suitOfCard = suitOfCard;
	}
	public double getSuitOfCard(){
		return this.suitOfCard;
	}
}